import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BootstrapcarouselComponent } from './bootstrapcarousel.component';

describe('BootstrapcarouselComponent', () => {
  let component: BootstrapcarouselComponent;
  let fixture: ComponentFixture<BootstrapcarouselComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BootstrapcarouselComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BootstrapcarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
