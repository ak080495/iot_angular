import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders,HttpClientXsrfModule } from '@angular/common/http';
import { User } from './user';
import { Observable } from 'rxjs';
import {CookieService} from 'ngx-cookie-service'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // private baseUrl = 'http://localhost:8000/api';
  private baseUrl = 'https://foodiesplace1.herokuapp.com';
  // httpheades = new HttpHeaders({'Content-type' : 'application/json'})
  cookieValue ="";
  today: number = Date.now();

  constructor(private cookie:CookieService,private http: HttpClient,private csrf: HttpClientXsrfModule) { }

  
createUser(user: User) {
    return this.http.post(`${this.baseUrl}/sign_up/`, user,

    );
  }

getAll() {
  return this.http.get<User[]>(`${this.baseUrl}/users/`);
}

getById(id: number) {
  return this.http.get(`${this.baseUrl}/users/${id}`);
}
delete(id: number)  {
  return this.http.delete(`${this.baseUrl}/users/${id}`);
}

getOneUser(id: number): Observable<Object> {
  return this.http.get(`${this.baseUrl}/users/${id}`);
}

updateUser(updateForm,id) {
  return this.http.put(`${this.baseUrl}/users/${id}/`, updateForm,

  );
}

public upload(formData: FormData,id: number) {
  return this.http.put<any>(`${this.baseUrl}/users/${id}/`, formData);
}
/////////////////////////change password/////////////
pwdChange(setpwdForm: any,id: number) {
  return this.http.put(`${this.baseUrl}/users/${id}/set_password/`, setpwdForm,

  );
}
/////////////////////reset password///////////////////
resetpwd(fgtpwdForm: any) {
  // this.cookie.set('csrftoken','vlhzDhPfU3CdfkX8i5EfQ8DlCCPpOkbCGzTNNol5gn863Vmz5ZMkEwC6Pvr2vHpp',this.today,'/','localhost',false,'Lax');

  // return this.http.post(`${this.baseUrl}/password/reset/`, fgtpwdForm,
  return this.http.post(`${this.baseUrl}/pwd/password/reset/`, fgtpwdForm,
  // return this.http.post(`${this.baseUrl}/password_reset/`, fgtpwdForm,

  
  // {
  //   headers: { 
  //     "X-CSRFToken":undefined
  //   },

    
    // Cookies.set('name', 'value', { secure: true })
    // withCredentials: true,
    // Cookies: any;
    // responseType:"arraybuffer",
    // params:

  // },

  );
}



resetUser(resetForm: any) {
  return this.http.post(`${this.baseUrl}/pwd/password/reset/confirm/`, resetForm,

  );
}




}
