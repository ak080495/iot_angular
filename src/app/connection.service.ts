// import { Injectable } from '@angular/core';

// @Injectable({
//   providedIn: 'root'
// })
// export class ConnectionService {

//   constructor() { }
// }


import { Messages } from './../app/message';


import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
providedIn: 'root'
})
export class ConnectionService {
// url: string = 'http://localhost:8000/contact/';
// private baseUrl = 'http://localhost:8000';
private baseUrl = 'https://foodiesplace1.herokuapp.com';

constructor(private http: HttpClient) { }

// sendMessage(messageContent: any) {
//   return this.http.post(this.url,
//   JSON.stringify(messageContent),
//   { headers: new HttpHeaders({ 'Content-Type': 'application/json' }), responseType: 'text' });
// }
// }

createUser(message: Messages) {

  return this.http.post(`${this.baseUrl}/contact/`, message);
}
}