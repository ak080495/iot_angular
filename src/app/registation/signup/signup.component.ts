// import { Component, OnInit } from '@angular/core';
// import { Router } from '@angular/router';
// import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { first } from 'rxjs/operators';

// import { AuthenticationService } from '../_services/authentication.service';
// import { UserService } from '../../user.service';
// import { AlertService,  } from './../_services/alert.service';


// @Component({templateUrl: './signup.component.html',})
// export class SignupComponent implements OnInit {
//     registerForm: FormGroup;
//     loading = false;
//     submitted = false;

//     constructor(
//         private formBuilder: FormBuilder,
//         private router: Router,
//         private authenticationService: AuthenticationService,
//         private userService: UserService,
//         private alertService: AlertService
//     ) { 
//         // redirect to home if already logged in
//         if (this.authenticationService.currentUserValue) { 
//             this.router.navigate(['/']);
//         }
//     }

//     ngOnInit() {
//         this.registerForm = this.formBuilder.group({
//             first_name: ['', Validators.required],
//             last_name: ['', Validators.required],
//             username: ['', Validators.required],
//             password1: ['', [Validators.required, Validators.minLength(6)]],
//             password2: ['', [Validators.required, Validators.minLength(6)]]
            

//         });
//     }

//     // convenience getter for easy access to form fields
//     get f() { return this.registerForm.controls; }

//     onSubmit() {
//         this.submitted = true;

//         // stop here if form is invalid
//         if (this.registerForm.invalid) {
//             return;
//         }

//         // this.loading = true;
//         this.userService.createUser(this.registerForm.value)
//             .pipe(first())
//             .subscribe(
//                 data => {
//                     this.alertService.success('Registration successful', true);
//                     // this.router.navigate(['/home']);
//                 },
//                 error => {
//                     this.alertService.error(error);
//                     this.loading = false;
//                 });
//     }
// }











































































































import {HttpInterceptor,HttpErrorResponse} from '@angular/common/http';

import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

import {MatSnackBar} from '@angular/material/snack-bar';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


import { User } from '../../user';
import { UserService } from '../../user.service';
import { AlertService,  } from './../_services/alert.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  registerForm: FormGroup;
  id: any;
  user: User = new User();
  submitted = false;
  constructor(public dialogRef: MatDialogRef<SignupComponent>,private snackBar: MatSnackBar,private alertService: AlertService,private router: Router,private formBuilder: FormBuilder,private userService: UserService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      first_name: ['', Validators.required],
      userame: ['', Validators.required],
      last_name: ['', Validators.required],
      from_email: ['', [Validators.required, Validators.email]],
      password1: ['', [Validators.required, Validators.minLength(6)]],
      password2: ['', [Validators.required, Validators.minLength(6)]]
  });
  }
  newUser(): void {
    this.submitted = false;
    this.user = new User();
  }
  
  save() {
    this.userService.createUser(this.user,)
      .subscribe(
        data => {
          console.log(data);
          // this.submitted = true;
          this.dialogRef.close();
          // this.alertService.success('Registration successful', true);
          //           this.router.navigate(['/']);
          this.snackBar.open('Account sussesfully created.', 'close',{
            duration: 2000,
          });

        },
        error => {
          console.log(error)
          // this.alertService.error(error);
          // alert('User name already registered.');
          this.snackBar.open('Username or email already registered.', 'close',{
            duration: 2000,
          });

        }
        
        );
        // alert('Your message has been sent.');
        // this.alertService.error(error);
    this.user = new User();
  }
  
  onSubmit() {
    // alert('Your message has been sent.');
    this.save();
  }



}
