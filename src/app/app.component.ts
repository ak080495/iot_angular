import { Component } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Router } from '@angular/router';


import { AuthenticationService } from '../app/registation/_services/authentication.service';
import { User } from '../app/user';
import {CookieService} from 'ngx-cookie-service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'nevigation';
  currentUser: User;

  cookieValue ="";
  today: number = Date.now();


  constructor(private cookie:CookieService,
    private router: Router,
    private authenticationService: AuthenticationService
) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    this.cookie.set('csrftoken','vlhzDhPfU3CdfkX8i5EfQ8DlCCPpOkbCGzTNNol5gn863Vmz5ZMkEwC6Pvr2vHpp',this.today,'/','localhost',false,'Lax');
    this.cookieValue = this.cookie.get('csrftoken');


}

logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
}
}

