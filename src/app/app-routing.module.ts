import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { MenuComponent } from './menu/menu.component';
import { AboutComponent } from './component/about/about.component';
import { GalleryComponent } from './component/gallery/gallery.component';
import { BlogComponent } from './component/blog/blog.component';
import { ContactComponent } from './component/contact/contact.component';
import { ItemdetailComponent } from './itemdetail/itemdetail.component';
import { InternalServerComponent} from '../app/error-pages/internal-server/internal-server.component';
import { from } from 'rxjs';
// import { SignupComponent } from './registation/signup/signup.component';

import { ProfileComponent } from './registation/profile/profile.component';
import { AssetComponent } from './asset/asset.component';


import { ReserconfirmComponent } from './reserconfirm/reserconfirm.component'
import { HealthComponent } from './health/health.component';
import { TrackComponent } from './track/track.component';

const routes: Routes = [
  { path: '500', component: InternalServerComponent },
  { path: '',component: HomeComponent },
  { path: 'menu/:field/:id',component: ItemdetailComponent },
  { path: 'home',component: HomeComponent },
  { path: 'menu', component: MenuComponent},
  { path: 'about', component: AboutComponent}, 
  { path:'asset',component: AssetComponent }, 
  { path: 'health',component: HealthComponent },
  { path: 'track', component:TrackComponent},  
  { path: 'careers', component: GalleryComponent},
  { path: 'blog', component: BlogComponent},
  { path: 'contact', component: ContactComponent},
  { path: 'profile/:id', component:  ProfileComponent},
  // { path: 'resetconfirm', component: ReserconfirmComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
