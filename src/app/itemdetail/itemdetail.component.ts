import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { ImageService } from '../image.service'; 

@Component({
  selector: 'app-itemdetail',
  templateUrl: './itemdetail.component.html',
  styleUrls: ['./itemdetail.component.scss']
})
export class ItemdetailComponent implements OnInit {




  interfacesBrief: any;
  constructor(private imageService: ImageService,private router: Router,private route: ActivatedRoute) { 
    
    
  }

  ngOnInit() {
    const routeParams = this.route.snapshot.params;
    this.imageService.getOneItem(routeParams.id,routeParams.field).subscribe (
      data => this.interfacesBrief = data,

    );

  }
  }


