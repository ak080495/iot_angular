import { Component, OnInit, HostListener, } from '@angular/core';
import { Renderer2, Inject } from '@angular/core';
import { trigger, state, transition, style, animate } from '@angular/animations'; 
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';


import {DOCUMENT} from '@angular/common';
 
 declare var jQuery: any;


import { from } from 'rxjs';


interface Place {
  imgSrc: string;
  name: string;
  description: string;
  charge: string;
  location: string;
}


interface Place1 {
  imgSrc1: string;
  name1: string;
  description: string;
  charge: string;
  location: string;
  abc: number;
}



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],

  animations:[ 
    trigger('fade',
    [ 
      state('void', style({ opacity : 0})),
      transition(':enter',[ animate(300)]),
      transition(':leave',[ animate(500)]),
    ]
)]


})
export class HomeComponent implements OnInit {

  
 



 







  title = 'Ng Image Slider';
  showSlider = true;
sliderWidth: Number = 1670;
sliderImageWidth: Number = 300;
sliderImageHeight: Number = 300;
sliderImageGap:Number = 0;
  sliderArrowShow: Boolean = true;
  sliderInfinite: Boolean = true;
  sliderImagePopup: Boolean = true;
  sliderAutoSlide: Boolean = true;
  sliderSlideImage: Number = 1;
  sliderAnimationSpeed: any = 1;


imageObject: Array<object> = [{
  image: '../../../assets/FotoJet2.jpg',
  thumbImage: '../../../assets/FotoJet2.jpg',
  alt: 'alt of image',
  title: 'title of image'
}, {
  image: '../../../assets/blue.jpg', // Support base64 image
  thumbImage: '../../../assets/blue.jpg', // Support base64 image
  title: 'Image title', //Optional: You can use this key if want to show image with title
  alt: 'Image alt' //Optional: You can use this key if want to show image with alt
},{
  image: '../../../assets/newname.jpg',
  thumbImage: '../../../assets/newname.jpg',
  alt: 'alt of image',
  title: 'title of image'
}, {
  image: '../../../assets/chocolate.jpg', // Support base64 image
  thumbImage: '../../../assets/chocolate.jpg', // Support base64 image
  title: 'Image title', //Optional: You can use this key if want to show image with title
  alt: 'Image alt' //Optional: You can use this key if want to show image with alt
},
{
  image: '../../../assets/filter.jpg',
  thumbImage: '../../../assets/filter.jpg',
  alt: 'alt of image',
  title: 'title of image'
}, {
  image: '../../../assets/brgr.JPG', // Support base64 image
  thumbImage: '../../../assets/brgr.JPG', // Support base64 image
  title: 'Image title', //Optional: You can use this key if want to show image with title
  alt: 'Image alt' //Optional: You can use this key if want to show image with alt
},{
  image: '../../../assets/hhh.JPG',
  thumbImage: '../../../assets/hhh.JPG',
  alt: 'alt of image',
  title: 'title of image'
}, {
  image: '../../../assets/brgrone.JPG', // Support base64 image
  thumbImage: '../../../assets/brgrone.JPG', // Support base64 image
  title: 'Image title', //Optional: You can use this key if want to show image with title
  alt: 'Image alt' //Optional: You can use this key if want to show image with alt
 },
// {
//   image: '../../../assets/card-1.gif',
//   thumbImage: '../../../assets/card-1.gif',
//   alt: 'alt of image',
//   title: 'title of image'
// }, {
//   image: '../../../assets/card-1.gif', // Support base64 image
//   thumbImage: '../../../assets/card-1.gif', // Support base64 image
//   title: 'Image title', //Optional: You can use this key if want to show image with title
//   alt: 'Image alt' //Optional: You can use this key if want to show image with alt
// },{
//   image: '../../../assets/card-1.gif',
//   thumbImage: '../../../assets/card-1.gif',
//   alt: 'alt of image',
//   title: 'title of image'
// }, {
//   image: '../../../assets/card-1.gif', // Support base64 image
//   thumbImage: '../../../assets/card-1.gif', // Support base64 image
//   title: 'Image title', //Optional: You can use this key if want to show image with title
//   alt: 'Image alt' //Optional: You can use this key if want to show image with alt
// },{
//   image: '../../../assets/card-1.gif',
//   thumbImage: '../../../assets/card-1.gif',
//   alt: 'alt of image',
//   title: 'title of image'
// }, 
{
  image: '../../../assets/FotoJet.jpg', // Support base64 image
  thumbImage: '../../../assets/FotoJet.jpg', // Support base64 image
  title: 'Image title', //Optional: You can use this key if want to show image with title
  alt: 'Image alt' //Optional: You can use this key if want to show image with alt
},
];



  dish: string;
  promotion: string;
  leader: string;
  dishErrMess: string;
  promotionErrMess: string;
  leaderErrMess: string;

  places: Array<Place> = [];
  places1: Array<Place1> = [];



  constructor(@Inject(DOCUMENT) document, private _bottomSheet: MatBottomSheet ) {}

  openBottomSheet(): void {
    this._bottomSheet.open(BottomSheetOverviewExampleSheet);
  }



  public setJsonLd(renderer2: Renderer2, data: any): void {

    
 
  }

  ngOnInit() {

    // function openForm() {
    //   document.getElementById("myFormm").style.display = "block";
    // }

   
  //   $("button").click(function(){
  //     $("iframe").each(function(){
  //         $(this).attr("src", $(this).data("src"));
  //     });
  // });











  



    var scroll = window.requestAnimationFrame ||
    // IE Fallback
    function(callback){ window.setTimeout(callback, 2000/60)};
var elementsToShow = document.querySelectorAll('.show-on-scroll'); 

function loop() {

Array.prototype.forEach.call(elementsToShow, function(element){
if (isElementInViewport(element)) {
element.classList.add('is-visible');
} else {
element.classList.remove('is-visible');
}
});

scroll(loop);
}

// Call the loop for the first time
loop();

// Helper function from: http://stackoverflow.com/a/7557433/274826
function isElementInViewport(el) {
// special bonus for those using jQuery
if (typeof jQuery === "function" && el instanceof jQuery) {
el = el[0];
}
var rect = el.getBoundingClientRect();
return (
(rect.top <= 0
&& rect.bottom >= 0)
||
(rect.bottom >= (window.innerHeight || document.documentElement.clientHeight) &&
rect.top <= (window.innerHeight || document.documentElement.clientHeight))
||
(rect.top >= 0 &&
rect.bottom <= (window.innerHeight || document.documentElement.clientHeight))
);
}





    
   





    this.places = [

      // assets/images/card-1.gif
      {
        imgSrc: 'assets/card-1.gif',
        name: 'Cozy 5 Stars Apartment',
        description: `The place is close to Barceloneta Beach and bus stop just 2 min by walk and near to "Naviglio"
              where you can enjoy the main night life in Barcelona.`,
        charge: '$899/night',
        location: 'Barcelona, Spain'
      },
      {
        imgSrc: 'assets/images/card-2.gif',
        name: 'Office Studio',
        description: `The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio"
              where you can enjoy the night life in London, UK.`,
        charge: '$1,119/night',
        location: 'London, UK'
      },
      {
        imgSrc: 'assets/images/card-3.gif',
        name: 'Beautiful Castle',
        description: `The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio"
              where you can enjoy the main night life in Milan.`,
        charge: '$459/night',
        location: 'Milan, Italy'
      }
    ];

    this.places1 = [

      // assets/images/card-1.gif
      {
        imgSrc1: 'assets/card-1.gif',
        name1: 'Cozy 5 Stars Apartment',
        description: `The place is close to Barceloneta Beach and bus stop just 2 min by walk and near to "Naviglio"
              where you can enjoy the main night life in Barcelona.`,
        charge: '$899/night',
        location: 'Barcelona, Spain',
        abc: 30
      },
      {
        imgSrc1: 'assets/images/card-2.gif',
        name1: 'Office Studio',
        description: `The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio"
              where you can enjoy the night life in London, UK.`,
        charge: '$1,119/night',
        location: 'London, UK',
        abc: 70
      },
      
    ];
  }

  @HostListener('window:scroll', ['$event'])
  onWindowScroll(e) {
     if (window.pageYOffset > 550) {
       let element = document.getElementById('navbar');
       element.classList.add('sticky');
     } else {
      let element = document.getElementById('navbar');
        element.classList.remove('sticky'); 
     }
  }

}

@Component({
  selector: 'bottom-sheet-overview-example-sheet',
  templateUrl: 'bottom-sheet-overview-example-sheet.html',
})
export class BottomSheetOverviewExampleSheet {
  constructor(private _bottomSheetRef: MatBottomSheetRef<BottomSheetOverviewExampleSheet>) {}

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }
}












