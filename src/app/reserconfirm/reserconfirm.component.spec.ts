import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReserconfirmComponent } from './reserconfirm.component';

describe('ReserconfirmComponent', () => {
  let component: ReserconfirmComponent;
  let fixture: ComponentFixture<ReserconfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReserconfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReserconfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
